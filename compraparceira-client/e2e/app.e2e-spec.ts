import { CompraparceiraClientPage } from './app.po';

describe('compraparceira-client App', function() {
  let page: CompraparceiraClientPage;

  beforeEach(() => {
    page = new CompraparceiraClientPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
