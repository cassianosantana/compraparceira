package br.com.compraparceira;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CompraparceiraApplication {

	public static void main(String[] args) {
		SpringApplication.run(CompraparceiraApplication.class, args);
	}
}
